#include "vectors.h"

#include <stdio.h>

#define SIZE 10

uint32_t uTable[SIZE] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

bool equal(uint32_t u1, uint32_t u2) { return (u1 == u2); }

int main() {
    u32vec uvec = u32vec_create(0);
    u32vec_print(uvec, "%u", NULL);
    printf(" size:%zu \n", uvec.capacity);
    
    u32vec_push_array(&uvec, uTable, SIZE);
    u32vec_print(uvec, "%u", NULL);
    printf(" size:%zu \n", uvec.capacity);

    u32vec uvec2 = u32vec_create(0);
    u32vec_print(uvec2, "%u", NULL);
    printf(" size:%zu \n", uvec2.capacity);
    
    u32vec_push_array(&uvec2, uTable, SIZE);
    u32vec_print(uvec2, "%u", NULL);
    printf(" size:%zu \n", uvec2.capacity);

    if (u32vec_eqv(uvec, uvec2, equal)) {
        printf("SAME\n");
    } else {
        printf("NOT THE SAME\n");
    }

    u32vec_insert_array(&uvec, 1,uTable, SIZE);
    /*u32vec_print(uvec, "%u", NULL);
    printf(" size:%u \n", uvec.memorySize);*/

    /*u32vec_insertvec(&uvec, 1, uvec2);
    u32vec_print(uvec, "%u", NULL);
    printf(" size:%u \n", uvec.memorySize);*/


    if (u32vec_eqv(uvec, uvec2, equal)) {
        printf("SAME\n");
    } else {
        printf("NOT THE SAME\n");
    }

    u32vec_free(&uvec);
    u32vec_free(&uvec2);
}
