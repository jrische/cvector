#include <stdint.h>
#include <stdio.h>

#include "vectors.h"
#include "macros.h"

enum {
    SIZE = SIZE_C(10),
};

AnyValue avtable[SIZE*2];

int main() {
    u32vec uvec = u32vec_create(SIZE);
    i32vec ivec = i32vec_create(SIZE);
    fltvec fvec = fltvec_create(SIZE);
    AnyValueVec avvec = AnyValueVec_create(SIZE);
    AnyValuePtrVec avptrvec = AnyValuePtrVec_create(SIZE);
    
    for (uint32_t i=0; i<(SIZE * 2); i++) {
        u32vec_push(&uvec, i);
        i32vec_push(&ivec, - ((int32_t)i));
        fltvec_push(&fvec, (float) i);

        avtable[i].uInteger = i;
        AnyValueVec_push(&avvec, avtable[i]);
        AnyValuePtrVec_push(&avptrvec, &avtable[i]);
    }

    AnyValueVec_insert(&avvec, 2, avtable[SIZE-1]);
    //AnyValuevec_insertvec(&avvec, 3, avvec);

    printf("uint32_t=");
    u32vec_print(uvec, "%u", NULL);
    printf("\nint32_t=");
    i32vec_print(ivec, "%i", NULL);
    printf("\nfloat=");
    fltvec_print(fvec, "%f", NULL);
    printf("\nAnyValue=");
    AnyValueVec_print(avvec, "%u", " ");
    printf("\nAnyValue*=");
    AnyValuePtrVec_print(avptrvec, "%p", " ");
    printf("\n");

    u32vec_free(&uvec);
    i32vec_free(&ivec);
    fltvec_free(&fvec);
    AnyValueVec_free(&avvec);
    AnyValuePtrVec_free(&avptrvec);
}
