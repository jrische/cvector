#ifndef MACROS_H_
#define MACROS_H_

#define PRIMITIVE_CAT(a, b) a##b
#define CAT(a, b) PRIMITIVE_CAT(a, b)
#define PRIMITIVE_CAT_(a, b) a##_##b
#define CAT_(a, b) PRIMITIVE_CAT_(a, b)

#define EVAL(...) __EVAL1024(__VA_ARGS__)
#define __EVAL1024(...) __EVAL512(__EVAL512(__VA_ARGS__))
#define __EVAL512(...) __EVAL256(__EVAL256(__VA_ARGS__))
#define __EVAL256(...) __EVAL128(__EVAL128(__VA_ARGS__))
#define __EVAL128(...) __EVAL64(__EVAL64(__VA_ARGS__))
#define __EVAL64(...) __EVAL32(__EVAL32(__VA_ARGS__))
#define __EVAL32(...) __EVAL16(__EVAL16(__VA_ARGS__))
#define __EVAL16(...) __EVAL8(__EVAL8(__VA_ARGS__))
#define __EVAL8(...) __EVAL4(__EVAL4(__VA_ARGS__))
#define __EVAL4(...) __EVAL2(__EVAL2(__VA_ARGS__))
#define __EVAL2(...) __EVAL1(__EVAL1(__VA_ARGS__))
#define __EVAL1(...) __VA_ARGS__

#define EMPTY()

#define DEFER(macro) macro EMPTY()

#define SIZE_C(c) CAT(CAT(UINT,__WORDSIZE),_C(c))

#endif // MACROS_H_
